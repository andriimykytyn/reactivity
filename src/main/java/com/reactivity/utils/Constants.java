package com.reactivity.utils;

public final class Constants {



    private Constants() {}

    /** CONFIG OPTIONS */
    public static final int HTTP_PORT = 8082;

    /** API Routes */
    public static final String API_GET = "/todos/:id";
    public static final String API_LIST_ALL = "/todos";
    public static final String API_GET_BY_DATE = "/todos/date";
    public static final String API_CREATE = "/todos";
    public static final String API_UPDATE = "/todos/:id";
    public static final String API_DELETE = "/todos/:id";
    public static final String API_DELETE_ALL = "/todos";
}
