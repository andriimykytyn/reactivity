package com.reactivity.entity;

import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDate;

public @Data @AllArgsConstructor class Todo {

    private Long id;
    private String title;
    //for ideas to improve task completion
    private String notes;
    //urgent, usual, low
    private TodoPriority priority;
    //true - task will be auto created each day, false = task is disposable
    private Boolean routine;
    private Boolean completed;
    private LocalDate createdAt;
    //if not specified - equals to created date
    private LocalDate accomplishmentDate;
    private Long categoryId; 

    public Todo(JsonObject json) { fromJson(json); }
    public Todo(String json) { fromJson(new JsonObject(json)); }

    private void fromJson(JsonObject json) {
        id = json.getLong("id");
        title = json.getString("title");
        notes = json.getString("notes", "");
        priority = TodoPriority.valueOf(json.getString("priority", TodoPriority.USUAL.toString()));
        routine = govnoBoolean(json, "routine");
        completed = govnoBoolean(json, "completed");
        if (json.containsKey("createdAt")) {
            createdAt = LocalDate.parse(
                    json.getString("createdAt").replace("Z", "")
            );
        }
        if (json.containsKey("accomplishmentDate")) {
            accomplishmentDate = LocalDate.parse(
                    json.getString("accomplishmentDate").replace("Z", "")
            );
        }
        categoryId = json.getLong("categoryId");
    }

    public JsonObject toJson() {
        JsonObject json = new JsonObject();
        json.put("id", id);
        json.put("title", title);
        json.put("notes", notes);
        json.put("priority", priority.toString());
        json.put("routine", routine);
        json.put("completed", completed);
        json.put("createdAt", String.valueOf(createdAt));
        json.put("accomplishmentDate", String.valueOf(accomplishmentDate));
        json.put("categoryId", categoryId);
        return json;
    }

    private boolean govnoBoolean(JsonObject o, String k) {
        try{
            return o.getBoolean(k);
        }catch (Exception ignored){
            return o.getInteger(k, 0) != 0;
        }
    }

}
