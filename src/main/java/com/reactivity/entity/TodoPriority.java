package com.reactivity.entity;

public enum TodoPriority {
    URGENT, USUAL, LOW
}
