package com.reactivity.entity;

import io.vertx.core.json.JsonObject;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Category {

    private Long id;
    private String name;

    public Category(JsonObject json) { fromJson(json); }
    public Category(String json) { fromJson(new JsonObject(json)); }

    private void fromJson(JsonObject json) {
        id = json.getLong("id");
        name = json.getString("name");
    }

    public JsonObject toJson() {
        JsonObject json = new JsonObject();
        json.put("id", id);
        json.put("name", name);
        return json;
    }





}
