package com.reactivity;

import com.reactivity.dao.CategoryDao;
import com.reactivity.dao.JdbcCategoryDao;
import com.reactivity.dao.JdbcTodoDao;
import com.reactivity.dao.TodoDao;
import com.reactivity.service.CategoryService;
import com.reactivity.service.TodoService;
import com.reactivity.utils.Constants;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.http.HttpMethod;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.CorsHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashSet;
import java.util.Set;

public class ReactivityVerticle extends AbstractVerticle {

    private static final Logger LOGGER = LoggerFactory.getLogger(ReactivityVerticle.class);

    public ReactivityVerticle() {
    }

    @Override
    public void start(Future<Void> startFuture) {

        long start = System.currentTimeMillis(); //seconds from app start counter
        Router router = Router.router(vertx); // router obj

        // CORS support
        enableCorsSupport(router);

        //retrieve data from body
        router.route().handler(BodyHandler.create());

        TodoDao jdbcDao = new JdbcTodoDao(vertx, config());
        TodoService todoService = new TodoService(jdbcDao);

        CategoryDao categoryDao = new JdbcCategoryDao(vertx, config());
        CategoryService categoryService = new CategoryService(categoryDao);

        router.get(Constants.API_GET_BY_DATE).handler(todoService::handleGetByDate);
        router.get(Constants.API_LIST_ALL).handler(todoService::handleGetAll);
        router.get(Constants.API_GET).handler(todoService::handleGetTodo);
        router.post(Constants.API_CREATE).handler(todoService::handleCreateTodo);
        router.put(Constants.API_UPDATE).handler(todoService::handleUpdateTodo);
        router.delete(Constants.API_DELETE).handler(todoService::handleDeleteOne);
        router.delete(Constants.API_DELETE_ALL).handler(todoService::handleDeleteAll);

        router.get("/category/:id").handler(categoryService::handleGet);
        router.get("/category").handler(categoryService::handleGetAll);
        router.post("/category").handler(categoryService::handleCreate);
        router.put("/category/:id").handler(categoryService::handleUpdate);
        router.delete("/category/:id").handler(categoryService::handleDeleteOne);
        router.delete("/category").handler(categoryService::handleDeleteAll);


        int httpPort = config().getInteger("http.port", Constants.HTTP_PORT);

        vertx.createHttpServer()
                .requestHandler(router::accept)
                .listen(httpPort,
                        result -> {
                            if (result.succeeded()) {
                                startFuture.complete();
                                LOGGER.info("\nhttp server started in {} seconds. Little hurray",
                                        (System.currentTimeMillis() - start) / 1000F);
                            } else {
                                startFuture.fail(result.cause());
                                LOGGER.error("Creating http server error. Message: {}. Cause: {}",
                                        result.cause(), result.cause().getLocalizedMessage());
                            }
                        });

    }

    /**
     * Enable CORS support for web router.
     *
     * @param router router instance
     */
    private void enableCorsSupport(Router router) {
        Set<String> allowHeaders = new HashSet<>();
        allowHeaders.add("x-requested-with");
        allowHeaders.add("Access-Control-Allow-Origin");
        allowHeaders.add("origin");
        allowHeaders.add("Content-Type");
        allowHeaders.add("accept");
        // CORS support
        router.route().handler(CorsHandler.create("*")
                .allowedHeaders(allowHeaders)
                .allowedMethod(HttpMethod.GET)
                .allowedMethod(HttpMethod.POST)
                .allowedMethod(HttpMethod.DELETE)
                .allowedMethod(HttpMethod.PATCH)
                .allowedMethod(HttpMethod.PUT)
        );
    }
}
