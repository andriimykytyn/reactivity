package com.reactivity.dao;

import com.reactivity.entity.Category;
import io.vertx.core.Future;
import io.vertx.core.json.JsonObject;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface CategoryDao {

    Future<Category> create(Category category);

    Future<List<Category>> getAll();

    Future<Optional<Category>> getCertain(String todoID);

    Future<Category> update(String id, Category category);

    Future<Boolean> delete(String id);

    Future<Boolean> deleteAll();

}
