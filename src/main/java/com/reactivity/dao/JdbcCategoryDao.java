package com.reactivity.dao;

import com.reactivity.entity.Category;
import io.vertx.core.*;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.asyncsql.MySQLClient;
import io.vertx.ext.sql.ResultSet;
import io.vertx.ext.sql.SQLClient;
import io.vertx.ext.sql.SQLConnection;
import io.vertx.ext.sql.UpdateResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class JdbcCategoryDao implements CategoryDao {

    private final SQLClient client;
    private static final Logger LOGGER = LoggerFactory.getLogger(JdbcCategoryDao.class);

    public JdbcCategoryDao(Vertx vertx, JsonObject config) {
        this.client = MySQLClient.createShared(vertx, config);
    }


    @Override
    public Future<Category> create(Category category) {
        return Future.future(promise ->
                executeUpdate(promise, SQL.INSERT,
                        new JsonArray()
                                .add(category.getName()), r -> {
                            if (r.failed()) {
                                LOGGER.error("Insert category {} fails. Cause: {}", category, r.cause().getMessage());
                                promise.fail(r.cause());
                            } else {
                                category.setId(r.result().getKeys().getLong(0));
                                promise.complete(category);
                            }
                        })
        );
    }

    @Override
    public Future<List<Category>> getAll() {
        return Future.future(promise ->
                executeQuery(promise, SQL.SELECT_ALL, r -> {
                    if (r.failed()) {
                        LOGGER.error("Get all fails. Cause: {}", r.cause().getMessage());
                        promise.fail(r.cause());
                    } else {
                        promise.complete(getMultiple(r.result()));
                    }
                })
        );
    }

    @Override
    public Future<Optional<Category>> getCertain(String todoID) {
        return Future.future(promise ->
                executePreparedQuery(promise, SQL.SELECT, new JsonArray().add(todoID), r -> {
                    if (r.failed()) {
                        LOGGER.error("Get category by id {} fails. Cause: {}", todoID, r.cause().getMessage());
                        promise.fail(r.cause());
                    } else {
                        promise.complete(getSingle(r.result()));
                    }
                })
        );
    }

    public Future<Category> update(String id, Category category) {
        return Future.future(promise ->
                executeUpdate(promise, SQL.UPDATE,
                        new JsonArray()
                                .add(category.getName())
                                .add(category.getId()), r -> {
                            if (r.failed()) {
                                LOGGER.error("UPDATE category {} fails. Cause: {}", category, r.cause().getMessage());
                                promise.fail(r.cause());
                            } else {
                                promise.complete(category);
                            }
                        })
        );
    }

    @Override
    public Future<Boolean> delete(String todoId) {
        return Future.future(promise ->
                executeUpdate(promise, SQL.DELETE, new JsonArray().add(todoId), r -> {
                    if (r.failed()) {
                        LOGGER.error("DELETE fails. Cause: {}", r.cause().getMessage());
                        promise.fail(r.cause());
                    } else {
                        promise.complete(true);
                    }
                })
        );
    }

    @Override
    public Future<Boolean> deleteAll() {
        return Future.future(promise ->
                executeQuery(promise, SQL.DELETE_ALL, r -> {
                    if (r.failed()) {
                        LOGGER.error("DELETE all fails. Cause: {}", r.cause().getMessage());
                        promise.fail(r.cause());
                    } else {
                        promise.complete(true);
                    }
                })
        );
    }

    private void executeUpdate(Promise failing, String sql, JsonArray params, Handler<AsyncResult<UpdateResult>> h) {
        client.getConnection(connHandler(failing, connection -> {
            connection.updateWithParams(sql, params, ar -> {
                h.handle(ar);
                connection.close();
            });
        }));
    }

    private void executePreparedQuery(Promise failing, String sql, JsonArray params, Handler<AsyncResult<ResultSet>> h) {
        client.getConnection(connHandler(failing, connection -> {
            connection.queryWithParams(sql, params, ar -> {
                h.handle(ar);
                connection.close();
            });
        }));
    }

    private void executeQuery(Promise failing, String sql, Handler<AsyncResult<ResultSet>> h) {
        client.getConnection(connHandler(failing, connection -> {
            connection.query(sql, ar -> {
                h.handle(ar);
                connection.close();
            });
        }));
    }

    private Handler<AsyncResult<SQLConnection>> connHandler(Promise future, Handler<SQLConnection> handler) {
        return conn -> {
            if (conn.succeeded()) {
                final SQLConnection connection = conn.result();
                handler.handle(connection);
            } else {
                LOGGER.error("Connection failed. Cause: {}", conn.cause().getMessage());
                future.fail(conn.cause());
            }
        };
    }

    private Optional<Category> getSingle(ResultSet from) {
        List<JsonObject> list = from.getRows();
        if (list == null || list.isEmpty()) {
            return Optional.empty();
        } else {
            return Optional.of(new Category(list.get(0)));
        }
    }

    private List<Category> getMultiple(ResultSet from) {
        List<JsonObject> list = from.getRows();
        return list.stream().map(Category::new).collect(Collectors.toList());
    }

    private static class SQL {
        static final String INSERT = "INSERT INTO `category` " +
                "(`name`) VALUES (?)";
        static final String SELECT = "SELECT * FROM category WHERE id = ?";
        static final String SELECT_ALL = "SELECT * FROM category";
        static final String UPDATE = "UPDATE category SET name=? WHERE id=?";
        static final String DELETE = "DELETE FROM `category` WHERE `id` = ?";
        static final String DELETE_ALL = "DELETE FROM `category`";
    }

}
