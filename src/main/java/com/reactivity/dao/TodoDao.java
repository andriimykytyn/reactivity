package com.reactivity.dao;

import com.reactivity.entity.Todo;
import com.reactivity.entity.Category;
import io.vertx.core.Future;
import io.vertx.core.json.JsonObject;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface TodoDao {

    Future<Todo> insert(Todo todo);

    Future<Todo> update(String todoId, Todo todo);

    Future<List<Todo>> getAll();

    Future<Optional<Todo>> getCertain(String todoID);

    Future<List<Todo>> getByDate(LocalDate dateStart, LocalDate dateEnd);

    Future<Boolean> delete(String todoId);

    Future<Boolean> deleteAll();
}
