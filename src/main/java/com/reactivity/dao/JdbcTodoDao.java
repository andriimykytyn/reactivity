package com.reactivity.dao;

import com.reactivity.entity.Todo;
import io.vertx.core.*;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.asyncsql.MySQLClient;
import io.vertx.ext.sql.ResultSet;
import io.vertx.ext.sql.SQLClient;
import io.vertx.ext.sql.SQLConnection;
import io.vertx.ext.sql.UpdateResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class JdbcTodoDao implements TodoDao {


    private final SQLClient client;
    private static final Logger LOGGER = LoggerFactory.getLogger(JdbcTodoDao.class);

    public JdbcTodoDao(Vertx vertx, JsonObject config) {
        this.client = MySQLClient.createShared(vertx, config);
    }

    @Override
    public Future<Todo> insert(Todo todo) {
        return Future.future(promise ->
            executeUpdate(promise, SQL.INSERT,
                    govnoArgs(new JsonArray()
                            .add(todo.getTitle())
                            .add(todo.getNotes())
                            .add(todo.getPriority().toString())
                            .add(todo.getRoutine())
                            .add(todo.getCompleted())
                            .add(String.valueOf(todo.getCreatedAt())) // FIXME set createdAt here?
                            .add(String.valueOf(todo.getAccomplishmentDate()))), r -> {
                    if (r.failed()) {
                        LOGGER.error("Insert todo {} fails. Cause: {}", todo, r.cause().getMessage());
                        promise.fail(r.cause());
                    } else {
                        System.out.println(todo.getAccomplishmentDate().toString());
                        todo.setId(r.result().getKeys().getLong(0));
                        promise.complete(todo);
                    }
                })
        );
    }

    @Override
    public Future<Todo> update(String todoId, Todo updated) {
        return Future.future(promise -> getCertain(todoId).setHandler(ar -> { // FIXME unnecessary?
            if (ar.succeeded()) {
                System.out.println(updated);
                executeUpdate(promise, SQL.UPDATE,
                        govnoArgs(new JsonArray()
                                .add(updated.getTitle())
                                .add(updated.getNotes())
                                .add(updated.getPriority().toString())
                                .add(updated.getRoutine())
                                .add(updated.getCompleted())
                                .add(updated.getAccomplishmentDate().toString())
                                .add(updated.getCategoryId())
                                .add(updated.getId())), r -> {
                            if (r.failed()) {
                                LOGGER.error("UPDATE todo {} fails. Cause: {}", updated, r.cause().getMessage());
                                promise.fail(r.cause());
                            } else {
                                promise.complete(updated);
                            }
                        });
            } else {
                LOGGER.error("find by id {} error", todoId);
            }
        }));
    }

    @Override
    public Future<List<Todo>> getAll() {
        return Future.future(promise ->
                executeQuery(promise, SQL.SELECT_ALL, r -> {
                    if (r.failed()) {
                        LOGGER.error("Get all fails. Cause: {}", r.cause().getMessage());
                        promise.fail(r.cause());
                    } else {
                        promise.complete(getMultiple(r.result()));
                    }
                })
        );
    }

    @Override
    public Future<List<Todo>> getByDate(LocalDate dateStart, LocalDate dateEnd) {
        return Future.future(promise ->
                executePreparedQuery(promise, SQL.SELECT_BY_DATE, govnoArgs(new JsonArray().add(String.valueOf(dateStart)).add(String.valueOf(dateEnd))), r -> {
                    if (r.failed()) {
                        LOGGER.error("Get by date fails. Cause: {}", r.cause().getMessage());
                        promise.fail(r.cause());
                    } else {
                        promise.complete(getMultiple(r.result()));
                    }
                })
        );
    }

    @Override
    public Future<Optional<Todo>> getCertain(String todoID) {
        return Future.future(promise ->
                executePreparedQuery(promise, SQL.SELECT, govnoArgs(new JsonArray().add(todoID)), r -> {
                    if (r.failed()) {
                        LOGGER.error("Get todo by id {} fails. Cause: {}", todoID, r.cause().getMessage());
                        promise.fail(r.cause());
                    } else {
                        promise.complete(getSingle(r.result()));
                    }
                })
        );
    }

    @Override
    public Future<Boolean> delete(String todoId) {
        return Future.future(promise ->
            executeUpdate(promise,SQL.DELETE, govnoArgs(new JsonArray().add(todoId)), r -> {
                if (r.failed()) {
                    LOGGER.error("DELETE fails. Cause: {}", r.cause().getMessage());
                    promise.fail(r.cause());
                } else {
                    promise.complete(true);
                }
            })
        );
    }

    @Override
    public Future<Boolean> deleteAll() {
        return Future.future(promise ->
        executeQuery(promise, SQL.DELETE_ALL, r -> {
                if (r.failed()) {
                    LOGGER.error("DELETE all fails. Cause: {}", r.cause().getMessage());
                    promise.fail(r.cause());
                } else {
                    promise.complete(true);
                }
            })
        );
    }

    private void executeUpdate(Promise failing, String sql, JsonArray params, Handler<AsyncResult<UpdateResult>> h) {
        client.getConnection(connHandler(failing, connection -> {
            connection.updateWithParams(sql, params, ar -> {
                h.handle(ar);
                connection.close();
            });
        }));
    }

    private void executePreparedQuery(Promise failing, String sql, JsonArray params, Handler<AsyncResult<ResultSet>> h) {
        client.getConnection(connHandler(failing, connection -> {
            connection.queryWithParams(sql, params, ar -> {
                h.handle(ar);
                connection.close();
            });
        }));
    }

    private void executeQuery(Promise failing, String sql, Handler<AsyncResult<ResultSet>> h) {
        client.getConnection(connHandler(failing, connection -> {
            connection.query(sql, ar -> {
                h.handle(ar);
                connection.close();
            });
        }));
    }

    private Handler<AsyncResult<SQLConnection>> connHandler(Promise future, Handler<SQLConnection> handler) {
        return conn -> {
            if (conn.succeeded()) {
                final SQLConnection connection = conn.result();
                handler.handle(connection);
            } else {
                LOGGER.error("Connection failed. Cause: {}", conn.cause().getMessage());
                future.fail(conn.cause());
            }
        };
    }

    private Optional<Todo> getSingle(ResultSet from){
        List<JsonObject> list = from.getRows();
        if (list == null || list.isEmpty()) {
            return Optional.empty();
        } else {
            return Optional.of(new Todo(list.get(0)));
        }
    }

    private List<Todo> getMultiple(ResultSet from){
        List<JsonObject> list = from.getRows();
        return list.stream().map(Todo::new).collect(Collectors.toList());
    }

    private static class SQL{
        static final String INSERT = "INSERT INTO `todo` " +
                "(`title`, `notes`, `priority`, `routine`, `completed`, `createdAt`, `accomplishmentDate`) VALUES (?, ?, ?, ?, ?, ?, ?)";
        static final String SELECT = "SELECT * FROM todo WHERE id = ?";
        static final String SELECT_ALL = "SELECT * FROM todo";
        static final String UPDATE = "UPDATE todo SET title=?, notes=?, priority=?, routine=?, completed=?, " +
                "accomplishmentDate=?, categoryId=? WHERE id=?";
        static final String DELETE = "DELETE FROM `todo` WHERE `id` = ?";
        static final String DELETE_ALL = "DELETE FROM `todo`";
        static final String SELECT_BY_DATE = "SELECT * FROM todo WHERE `accomplishmentDate` BETWEEN ? AND ?";
//        static final String ADD_TO_CATEGORY = "INSERT INTO `todo_has_category` (`todo_id`, `category_id`) VALUES (?, ?)";
//        static final String GET_BY_CATEGORY = "SELECT * FROM `todo` WHERE `id` IN ( " +
//                    "SELECT `todo_id` FROM `todo_has_category` WHERE `category_id` = ? " +
//                ")";
//        static final String GET_CATEGORY_BY_TODO_ID = "SELECT * FROM `category` WHERE `id` IN ( " +
//                    "SELECT `category_id` FROM `todo_has_category` WHERE `todo_id` = ? )";
    }

    public JsonArray govnoArgs(JsonArray govno) {
        return new JsonArray(govno.stream().map(o -> (o instanceof Boolean) ? ( ((Boolean)o)?1:0 ) : o).collect(Collectors.toList()));
    }

}
