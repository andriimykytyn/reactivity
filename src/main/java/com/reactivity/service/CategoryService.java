package com.reactivity.service;

import com.reactivity.dao.CategoryDao;
import com.reactivity.entity.Category;
import com.reactivity.entity.Todo;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.DecodeException;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class CategoryService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CategoryService.class);
    private final CategoryDao dao;

    public CategoryService(CategoryDao dao) {
        this.dao = dao;
    }


    public void handleCreate(RoutingContext context) {
        try {
            final Category todo = new Category(context.getBodyAsJson());

            dao.create(todo).setHandler(resultHandler(context, result -> {
                if (result != null) {
                    todo.setId(result.getId());
                    String encoded = Json.encodePrettily(todo.toJson());
                    context.response()
                            .setStatusCode(201)
                            .putHeader("content-type", "application/json")
                            .end(encoded);
                } else {
                    serviceUnavailable(context);
                }
            }));
        } catch (DecodeException e) {
            LOGGER.error("DecodeException. Cause: {}", e.getMessage());
            sendError(400, context.response());
        }
    }

    public void handleUpdate(RoutingContext context) {
        try {
            String todoId = context.request().getParam("id");
            JsonObject json = context.getBodyAsJson();
            // handle error
            if (todoId == null) {
                LOGGER.error("Request param todoId is null");
                sendError(400, context.response());
                return;
            }
            dao.update(todoId, new Category(json))
                    .setHandler(resultHandler(context, result -> {
                        if (result == null) {
                            LOGGER.error("Entity not found by id: {}", todoId);
                            notFound(context);
                        } else {
                            final String encoded = Json.encodePrettily(result.toJson());
                            context.response()
                                    .putHeader("content-type", "application/json")
                                    .end(encoded);
                        }
                    }));
        } catch (DecodeException e) {
            LOGGER.error("Decode error. {}", e.getMessage());
            badRequest(context);
        }
    }

    public void handleDeleteOne(RoutingContext context) {
        String todoID = context.request().getParam("id");
        dao.delete(todoID)
                .setHandler(deleteResultHandler(context));
    }

    public void handleDeleteAll(RoutingContext context) {
        dao.deleteAll()
                .setHandler(deleteResultHandler(context));
    }

    public void handleGet(RoutingContext context) {
        String id = context.request().getParam("id");
        if (id == null) {
            sendError(400, context.response());
            return;
        }

        dao.getCertain(id).setHandler(resultHandler(context, result -> {
            if (result.isEmpty()) {
                LOGGER.error("Entity not found.");
                notFound(context);
            } else {
                LOGGER.info("TODO: {}", result);
                final String encoded = Json.encodePrettily(result.get().toJson());
                context.response()
                        .putHeader("content-type", "application/json")
                        .end(encoded);
            }
        }));
    }

    public void handleGetAll(RoutingContext context) {
        dao.getAll().setHandler(resultHandler(context, result -> {
            if (result == null) {
                LOGGER.error("Service unavailable.");
                serviceUnavailable(context);
            } else {
                List<JsonObject> parsed = result.stream().map(Category::toJson).collect(Collectors.toList());
                final String encoded = Json.encodePrettily(parsed);
                context.response()
                        .putHeader("content-type", "application/json")
                        .end(encoded);
            }
        }));
    }

    private <T> Handler<AsyncResult<T>> resultHandler(RoutingContext context, Consumer<T> consumer) {
        return result -> {
            if (result.succeeded()) {
                consumer.accept(result.result());
            } else {
                LOGGER.error("Service unavailable. {}", result.cause().getMessage());
                serviceUnavailable(context);
            }
        };
    }

    private Handler<AsyncResult<Boolean>> deleteResultHandler(RoutingContext context) {
        return result -> {
            if (result.succeeded()) {
                if (result.result()) {
                    context.response().setStatusCode(204).end();
                } else {
                    LOGGER.error("Service unavailable. {}", result.cause().getMessage());
                    serviceUnavailable(context);
                }
            } else {
                LOGGER.error("Service unavailable. {}", result.cause().getMessage());
                serviceUnavailable(context);
            }
        };
    }

    private void sendError(int statusCode, HttpServerResponse response) {
        response.setStatusCode(statusCode).end();
    }

    private void badRequest(RoutingContext context) {
        context.response().setStatusCode(400).end();
    }

    private void notFound(RoutingContext context) {
        context.response().setStatusCode(404).end();
    }

    private void serviceUnavailable(RoutingContext context) {
        context.response().setStatusCode(503).end();
    }
}
