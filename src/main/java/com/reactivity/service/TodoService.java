package com.reactivity.service;

import com.reactivity.dao.TodoDao;
import com.reactivity.entity.Todo;
import com.reactivity.entity.Category;

import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.DecodeException;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.time.LocalDate;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class TodoService {

    private final TodoDao todoDao;
    private static final Logger LOGGER = LoggerFactory.getLogger(TodoService.class);

    public TodoService(TodoDao todoDao) {
        this.todoDao = todoDao;
    }

    public void handleCreateTodo(RoutingContext context) {
        try {
            final Todo todo = new Todo(context.getBodyAsJson());
            todo.setCreatedAt(LocalDate.now());
            String accomplishmentDate = context.getBodyAsJson().getString("accomplishmentDate", String.valueOf(LocalDate.now()));
            todo.setAccomplishmentDate(LocalDate.parse(accomplishmentDate));

            todoDao.insert(todo).setHandler(resultHandler(context, result -> {
                if (result != null) {
                    todo.setId(result.getId());
                    String encoded = Json.encodePrettily(todo.toJson());
                    context.response()
                            .setStatusCode(201)
                            .putHeader("content-type", "application/json")
                            .end(encoded);
                } else {
                    serviceUnavailable(context);
                }
            }));
        } catch (DecodeException e) {
            LOGGER.error("DecodeException. Cause: {}", e.getMessage());
            sendError(400, context.response());
        }
    }

    public void handleGetTodo(RoutingContext context) {
        String todoID = context.request().getParam("id");
        if (todoID == null) {
            sendError(400, context.response());
            return;
        }

        todoDao.getCertain(todoID).setHandler(resultHandler(context, result -> {
            if (result.isEmpty()) {
                LOGGER.error("Entity not found.");
                notFound(context);
            } else {
                LOGGER.info("TODO: {}", result);
                final String encoded = Json.encodePrettily(result.get().toJson());
                context.response()
                        .putHeader("content-type", "application/json")
                        .end(encoded);
            }
        }));
    }

    public void handleGetAll(RoutingContext context) {
        todoDao.getAll().setHandler(resultHandler(context, result -> {
            if (result == null) {
                LOGGER.error("Service unavailable.");
                serviceUnavailable(context);
            } else {
                List<JsonObject> parsed = result.stream().map(Todo::toJson).collect(Collectors.toList());
                final String encoded = Json.encodePrettily(parsed);
                context.response()
                        .putHeader("content-type", "application/json")
                        .end(encoded);
            }
        }));
    }

    public void handleGetByDate(RoutingContext context) {
        String date = context.request().getParam("from");
        String untilDate = context.request().getParam("until");
        if(untilDate == null) {
            untilDate = date;
        }
        if (date != null) {
            todoDao.getByDate(LocalDate.parse(date), LocalDate.parse(untilDate)).setHandler(resultHandler(context, result -> {
                if (result == null) {
                    LOGGER.error("Service unavailable.");
                    serviceUnavailable(context);
                } else {
                    List<JsonObject> parsed = result.stream().map(Todo::toJson).collect(Collectors.toList());
                    final String encoded = Json.encodePrettily(parsed);
                    context.response()
                            .putHeader("content-type", "application/json")
                            .end(encoded);
//                    List<Todo> completed = result.stream().filter(Todo::getCompleted).collect(Collectors.toList());
//                    double performance = ((double) completed.size() / result.size())*100;
//                    JsonObject res = new JsonObject()
//                            .put("size", result.size())
//                            .put("completed", completed.size())
//                            .put("performance", performance);
//                    final String encoded = Json.encodePrettily(res);
//                    context.response()
//                            .putHeader("content-type", "application/json")
//                            .end(encoded);
                }
            }));
        } else {
            context.response().setStatusCode(404);
        }
    }

    public void handleUpdateTodo(RoutingContext context) {
        try {
            String todoId = context.request().getParam("id");
            JsonObject json = context.getBodyAsJson();
            // handle error
            if (todoId == null) {
                LOGGER.error("Request param todoId is null");
                sendError(400, context.response());
                return;
            }
            todoDao.update(todoId, new Todo(json))
                    .setHandler(resultHandler(context, result -> {
                        if (result == null) {
                            LOGGER.error("Entity not found by id: {}", todoId);
                            notFound(context);
                        } else {
                            final String encoded = Json.encodePrettily(result.toJson());
                            context.response()
                                    .putHeader("content-type", "application/json")
                                    .end(encoded);
                        }
                    }));
        } catch (DecodeException e) {
            LOGGER.error("Decode error. {}", e.getMessage());
            badRequest(context);
        }
    }

    public void handleDeleteOne(RoutingContext context) {
        String todoID = context.request().getParam("id");
        todoDao.delete(todoID)
                .setHandler(deleteResultHandler(context));
    }

    public void handleDeleteAll(RoutingContext context) {
        todoDao.deleteAll()
                .setHandler(deleteResultHandler(context));
    }

    private <T> Handler<AsyncResult<T>> resultHandler(RoutingContext context, Consumer<T> consumer) {
        return result -> {
            if (result.succeeded()) {
                consumer.accept(result.result());
            } else {
                LOGGER.error("Service unavailable. {}", result.cause().getMessage());
                serviceUnavailable(context);
            }
        };
    }

    private Handler<AsyncResult<Boolean>> deleteResultHandler(RoutingContext context) {
        return result -> {
            if (result.succeeded()) {
                if (result.result()) {
                    context.response().setStatusCode(204).end();
                } else {
                    LOGGER.error("Service unavailable. {}", result.cause().getMessage());
                    serviceUnavailable(context);
                }
            } else {
                LOGGER.error("Service unavailable. {}", result.cause().getMessage());
                serviceUnavailable(context);
            }
        };
    }

    private void sendError(int statusCode, HttpServerResponse response) {
        response.setStatusCode(statusCode).end();
    }

    private void badRequest(RoutingContext context) {
        context.response().setStatusCode(400).end();
    }

    private void notFound(RoutingContext context) {
        context.response().setStatusCode(404).end();
    }

    private void serviceUnavailable(RoutingContext context) {
        context.response().setStatusCode(503).end();
    }

}
