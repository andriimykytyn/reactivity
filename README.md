## Reactivity 
web-application for organizing your time and facilitating
tracking time for self-awareness purposes on the way to your goals.

### Environment
- Java 11
- Maven 3 
- Docker / docker-compose

### Installation 

- use: ``git clone https://gitlab.com/andriimykytyn/reactivity.git`` 
to get this app in your desired folder.
- type in terminal ``cd reactivity``
- building sources ``mvn clean install`` from project root folder
- running app ``java -Dfile.encoding=UTF-8 -jar ~/reactivity/target/reactivity-1.0-SNAPSHOT-jar-with-dependencies.jar run com.reactivity.ReactivityVerticle -conf config/config.json``

### Configuration

You may want to configure this application to your specific port/host settings by changing: 
``~/reactivity/config/config.json``.

This is a default local configuration, you should change ``redis.host`` and ``http.port`` to your environment
 specific values``{
    "redis.host": "172.21.0.2",
    "redis.port": 6379,
    "redis.todo.key": "VERT_TODO",
    "http.port": 8082
  }``
  
### Docker

Run ``docker-compose up`` from the path ``~/reactivity/`` where docker-compose.yml is placed.

Configuration changes are available by path ``~/reactivity/config/dockerconfig.json``

default config: 
``{
    "redis.host": "redis",
    "redis.port": 6379,
    "redis.todo.key": "VERT_TODO",
    "http.port": 8080
  }``
  



