-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema reactivity
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema reactivity
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `reactivity` DEFAULT CHARACTER SET utf8 ;
USE `reactivity` ;

-- -----------------------------------------------------
-- Table `reactivity`.`todo`
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS `reactivity`.`todo` (
  `id` BIGINT(11) NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(50) NOT NULL,
  `notes` VARCHAR(250) NULL,
  `priority` ENUM('URGENT', 'USUAL', 'LOW') NOT NULL,
  `routine` TINYINT(1) NOT NULL,
  `completed` TINYINT(1) NOT NULL,
  `createdAt` DATE NOT NULL,
  `accomplishmentDate` DATE NOT NULL,
  `categoryId` BIGINT(11) NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_todo_has_category`
    FOREIGN KEY (`categoryId`)
    REFERENCES `reactivity`.`category` (`id`)
    ON DELETE SET NULL
    ON UPDATE CASCADE,
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `reactivity`.`goal`
-- -----------------------------------------------------
-- CREATE TABLE IF NOT EXISTS `reactivity`.`goal` (
--   `id` BIGINT(11) NOT NULL AUTO_INCREMENT,
--   `name` VARCHAR(100) NOT NULL,
--   `purpose` VARCHAR(400) NOT NULL,
--   `accomplishmentDate` DATE NOT NULL,
--   `achieved` TINYINT(1) NOT NULL,
--   PRIMARY KEY (`id`),
--   UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE)
-- ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `reactivity`.`category`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `reactivity`.`category` (
  `id` BIGINT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) UNIQUE NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `reactivity`.`todo_has_category`
-- -----------------------------------------------------
-- CREATE TABLE IF NOT EXISTS `reactivity`.`todo_has_category` (
--   `todo_id` BIGINT(11) NOT NULL,
--   `category_id` BIGINT(11) NOT NULL,
--   PRIMARY KEY (`todo_id`, `category_id`),
--   INDEX `fk_todo_has_category_category1_idx` (`category_id` ASC) VISIBLE,
--   INDEX `fk_todo_has_category_todo_idx` (`todo_id` ASC) VISIBLE,
  -- CONSTRAINT `fk_todo_has_category_todo`
  --   FOREIGN KEY (`todo_id`)
  --   REFERENCES `reactivity`.`todo` (`id`)
  --   ON DELETE CASCADE
  --   ON UPDATE CASCADE,
--   CONSTRAINT `fk_todo_has_category_category1`
--     FOREIGN KEY (`category_id`)
--     REFERENCES `reactivity`.`category` (`id`)
--     ON DELETE CASCADE
--     ON UPDATE CASCADE)
-- ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `reactivity`.`todo_has_goal`
-- -----------------------------------------------------
-- CREATE TABLE IF NOT EXISTS `reactivity`.`todo_has_goal` (
--   `todo_id` BIGINT(11) NOT NULL,
--   `goal_id` BIGINT(11) NOT NULL,
--   PRIMARY KEY (`todo_id`, `goal_id`),
--   INDEX `fk_todo_has_goal_goal1_idx` (`goal_id` ASC) VISIBLE,
--   INDEX `fk_todo_has_goal_todo1_idx` (`todo_id` ASC) VISIBLE,
--   CONSTRAINT `fk_todo_has_goal_todo1`
--     FOREIGN KEY (`todo_id`)
--     REFERENCES `reactivity`.`todo` (`id`)
--     ON DELETE NO ACTION
--     ON UPDATE NO ACTION,
--   CONSTRAINT `fk_todo_has_goal_goal1`
--     FOREIGN KEY (`goal_id`)
--     REFERENCES `reactivity`.`goal` (`id`)
--     ON DELETE NO ACTION
--     ON UPDATE NO ACTION)
-- ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
